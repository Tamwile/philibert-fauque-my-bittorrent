#!/bin/bash

if [ "$2" == "" ]; then
    find 1> "../""$1" 2> /dev/null
    find
else
    find $2 1> "../""$1" 2> /dev/null
    find $2
fi

if [ "$?" -eq 0 ]; then
    exit 0
fi

exit 1
