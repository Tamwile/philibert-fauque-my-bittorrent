# include <dirent.h>
# include <err.h>
# include <errno.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <unistd.h>

# include "tests_suite.h"

void rm_and_out(int ite, void *dir)
{
  if (ite)
  {
    fprintf(stderr, "SUPPRESSION DU TEST: %s\n", (char *) dir);
    char *const cmd[] = { "-rf", "-r",  (char *) dir, (char *) 0 };
    pid_t pid = fork();
    if (pid == -1)
	err(1, "RM_DIR fork error");
    if (pid == 0)
    {
      int out = execve(RM_DIR, cmd, NULL);
      if (out == -1)
	err(1, RM_DIR);
    }
    else
    {
      int stat;
      do {
	waitpid(pid, &stat, WUNTRACED);
      } while (!WIFEXITED(stat) && !WIFSIGNALED(stat));
    }
  }
}

DIR *get_main_dir(void)
{
  DIR *tests = opendir(PATH_TEST);
  if (!tests)
  {
    if (errno == ENOENT)
    {
      if (mkdir(PATH_TEST, S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH) == -1)
	err(1, PATH_TEST);
      if (!(tests = opendir(PATH_TEST)))
	err(1, PATH_TEST);
    }
    else
      err(1, PATH_TEST);
  }

  return tests;
}

void get_last_test(DIR *tests, unsigned *ite,
		   char name[SUB_DIR_LEN + MAX_POW_NB_TEST])
{
  struct dirent *sub_dir = readdir(tests);
  for (; sub_dir; sub_dir = readdir(tests))
  {
    snprintf(name, SUB_DIR_LEN + MAX_POW_NB_TEST, "%s%d", SUB_DIR, *ite);
    if (!strcmp(name, sub_dir->d_name))
    {
      tests = opendir(PATH_TEST);
      (*ite)++;
    }
  }
}

void fill_arg_file(int len, char *new_test, int arg, char **args)
{
  char file_args[len];
  snprintf(file_args, len, "%s/%s", new_test, FILE_ARGS);
  FILE *f_arg = fopen(file_args, "w");
  if (!f_arg)
    err(1, file_args);
  
  if (arg > 1)
  {
    if (fprintf(f_arg, args[1]) < 0)
      err(1, file_args);
  }

  if (fclose(f_arg) == EOF)
    err(1, file_args);
}

void fill_out_file(int len, char *new_test, int nb_arg, char **args)
{
  char file_out[len];
  snprintf(file_out, len, "%s/%s", new_test, FILE_OUT);
  FILE *f_out = fopen(file_out, "w");
  if (!f_out)
    err(1, file_out);
  
  if (fclose(f_out) == EOF)
    err(1, file_out);
  
  char *arguments[3 + nb_arg];
  arguments[0] = file_out; //Nom du script bash
  arguments[1] = file_out; //Fichier de sortie
  arguments[2] = ""; //si aucun argument
  arguments[2 + nb_arg] = (char*)0; //Fin des arguments
  if (nb_arg > 1)
  {
    arguments[2] = args[1];
  }
  
  pid_t pid = fork();
  if (pid == -1)
    err(1, "ADD_OUT_SH fork error");
  if (pid == 0)
  {
    chdir("folder_test");
    int out = execve(ADD_OUT_SH, arguments, NULL);
    if (out == -1)
      err(1, ADD_OUT_SH);
    if (out == 0)
    {
      fprintf(stderr, "%s failed\n", ADD_OUT_SH);
      exit(1);
    }
  }
  else
  {
    int stat;
    do {
      waitpid(pid, &stat, WUNTRACED);
    } while (!WIFEXITED(stat) && !WIFSIGNALED(stat));
  }
}

void fill_ret_file(int len, char *new_test, int nb_arg, char **args)
{
  char file_ret[len];
  snprintf(file_ret, len, "%s/%s", new_test, FILE_RET);
  FILE *f_ret = fopen(file_ret, "w");
  if (!f_ret)
    err(1, file_ret);
  
  if (nb_arg > 2)
  {
    if (fprintf(f_ret, args[2]) < 0)
      err(1, file_ret);
  }
  else
  {
    if (fprintf(f_ret, "NONE") < 0)
      err(1, file_ret);
  }

  if (fclose(f_ret) == EOF)
    err(1, file_ret);
}

int main(int nb_arg, char **args)
{
  if (nb_arg > 3)
  {
    errno = EINVAL;
    fprintf(stderr, "usage: ./add_test \"[args]\" [return value]\n");
    exit(1);
  }

  DIR *tests = get_main_dir();

  int len = SUB_DIR_LEN + MAX_POW_NB_TEST;
  char name[len];
  unsigned ite = 0;
  get_last_test(tests, &ite, name);
  
  if (closedir(tests) == -1)
    err(0, PATH_TEST);

  len += PATH_LEN;
  char new_test[len];
  snprintf(new_test, len, "%s/%s", PATH_TEST, name);
  if (mkdir(new_test, S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH) == -1)
    err(1, new_test);
  on_exit(rm_and_out, new_test);
  
  fill_arg_file(len + ARGS_LEN, new_test, nb_arg, args);
  fill_out_file(len + OUT_LEN, new_test, nb_arg, args);

  fill_ret_file(len + RET_LEN, new_test, nb_arg, args);
  
  return 0;
}
