#ifndef TESTS_SUITE_H
# define TESTS_SUITE_H

# define PATH_TEST "test_unit"
# define PATH_LEN 11

# define SUB_DIR "test"
# define SUB_DIR_LEN 4
# define MAX_POW_NB_TEST 4

# define FILE_ARGS "args"
# define ARGS_LEN 4

# define FILE_RET "return"
# define RET_LEN 6

# define FILE_OUT "out"
# define OUT_LEN 3

# define ADD_OUT_SH "fill_out.sh"
# define RM_DIR "/usr/bin/rm"

void rm_and_out(int ite, void *dir);
DIR *get_main_dir(void);
void get_last_test(DIR *tests, unsigned *ite,
		   char name[SUB_DIR_LEN + MAX_POW_NB_TEST]);
void fill_arg_file(int len, char *new_test, int arg, char **args);
void fill_out_file(int len, char *new_test, int arg, char **args);

#endif /* !TESTS_SUITE_H */
