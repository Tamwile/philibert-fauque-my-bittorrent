#!/bin/bash

cd "folder_test"

for dir in "../test_unit/test"* ; do
    if [ -d "$dir" ]; then
	arg=$(cat "$dir""/args")
        if [ "$arg" == "" ]; then
	    find 1> "$dir""/out" 2> /dev/null
	else
	    find $arg 1> "$dir""/out" 2> /dev/null
	fi
    fi
done

cd "../"
