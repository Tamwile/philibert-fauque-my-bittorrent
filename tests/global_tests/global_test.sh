#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
ONYELLOW='\033[43m'
UBLUE='\033[4;34m'
UCYAN='\033[4;96m'
ICYAN='\033[0;96m'
NC='\033[0m'

printf "${UCYAN}\nGLOBAL TEST SUITE\n${NC}"

verb=1
if [ "$#" -gt 0 ] && [ $1 == "-v" ]; then
    verb=0
fi

nb_test=0
sucess=0

cd "tests/global_tests"

for dir in "test_unit/test"* ; do
    if [ -d "$dir" ]; then
	
	$(cat "$dir""/args") 1> "test_out" 2> "err"
	ret="$?"
	diff "$dir""/out" "test_out" &> /dev/null
	err="$?"
	
	if [ "$err" -eq 0 ]; then
	    sucess=$((sucess + 1))
	    if [ "$verb" -eq 0 ]; then
		printf "${PURPLE}$dir""${NC}: ${GREEN}SUCCESS${NC}\n"
		if [ ! "$ret" -eq 0 ]; then
		    echo ">>"$( cat "$dir""/args" )
		    cat "err"
		    echo
		fi
	    fi
	else
	    printf "${PURPLE}$dir""${NC}: ${RED}FAILED${NC}\n"
	    echo ">>"$( cat "$dir""/args" )
	    if [ "$verb" -eq 0 ]; then
		diff "$dir""/out" "test_out"
		cat "err"
		echo
	    fi
	fi
	
	nb_test=$((nb_test + 1))
    fi
done

if [ ! "$nb_test" -eq 0 ]; then
    rm "test_out"
    rm "err"
fi

cd "../../"

printf "${ICYAN} ""$sucess"" successful tests out of ""$nb_test""\n\n${NC}"
exit 0
