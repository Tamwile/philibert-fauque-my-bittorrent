
# include "../src/flex.c"

#define main no__main
# include "../src/mybittorrent.c"
#undef main

extern struct info g_info;

int main(void)
{
  char *argv[] = {
    "--verbose",
    "--pretty-print-torrent-file",
    "--seed",
    "--debug",
    "test.torrent"
  };

  int nb = 5;
  
  char **torrent = read_option(&nb, argv);
  if (!torrent)
    aff_err("return is null");

  if (nb != 1)
  {
    printf("argc => %d\n", nb);
    aff_err("bad argc return");
  }
  
  if (argv[4] != *torrent)
  {
    printf("return => %s\n", *torrent);
    aff_err("bad return");
  }

  if (g_info.json != 1)
    aff_err("bad json initialisation");
  if (g_info.dpeers != 0)
    aff_err("bad dpeers initialisation");
  if (g_info.verbose != 1)
    aff_err("bad verbose initialisation");
  if (g_info.seed != 1)
    aff_err("bad seed initialisation");
  if (g_info.debug != 1)
    aff_err("bad debug initialisation");
  
  return 0;
}
