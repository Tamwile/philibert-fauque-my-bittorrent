# include "../src/flex.c"

#define main no__main
# include "../src/file_system.c"
# include "../src/mybittorrent.c"
# include "../src/str.c"
# include "../src/bencode.c"
# include "../src/tracker.c"
# include "../src/peers.c"
# include "../src/sha1.c"
# include "../src/read_torrent.c"
//# include "../src/socket.c"
#include <curl/curl.h>
#undef main

int main(int argc, char **argv)
{
  argc--;
  char **torrent = read_option(&argc, argv + 1);
  if (!torrent)
    return 1;

  struct info_dictio dico;
  struct bencode *dictionary = analyse_torrent(*torrent, &dico);
  char *info = get_info_char(*torrent, dico);

  struct torrent trnt =
  {
    .peer_id = "-MB2020-PFandCRcorpo",
    .torrent_id = { 0 }
  };
  g_info.torrent = &trnt;
  struct tracker_get_request *trck = send_tracker_GET_request(dictionary, info,
							      dico.length_dico,
							      &trnt);
  free(trck);
  free_bencode(dictionary, 0);
  return 0;
}



