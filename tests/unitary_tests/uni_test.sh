#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
ONYELLOW='\033[43m'
UBLUE='\033[4;34m'
UCYAN='\033[4;96m'
ICYAN='\033[0;96m'
NC='\033[0m'

printf "${UCYAN}\nUNITARY TEST SUITE\n${NC}"

verb=1
if [ "$#" -gt 0 ] && [ $1 == "-v" ]; then
    verb=0
fi

cd "tests/unitary_tests/"

if [ ! -d "bin" ]; then
    mkdir "bin"
fi

cp "../../src/" ./ -r

for dir in "main/"*".c" ; do
    if [ -f "$dir" ]; then
	make "${dir%.c}" 1> /dev/null
	mv "${dir%.c}" bin/
    fi
done

nb_test=0
sucess=0

for dir in "bin/"* ; do
    if [ -f "$dir" ]; then
	"$dir"
	ret="$?"
	
	if [ "$ret" -eq 0 ]; then
	    sucess=$((sucess + 1))
	    if [ "$verb" -eq 0 ]; then
		printf "${PURPLE}$dir""${NC}: ${GREEN}SUCCESS${NC}\n"
		if [ ! "$ret" -eq 0 ]; then
		    echo ">>""$dir"
		fi
	    fi
	else
	    printf "${PURPLE}$dir""${NC}: ${RED}FAILED${NC}\n"
	    echo ">>""$dir"
	fi
	
	nb_test=$((nb_test + 1))
    fi
done

cd "../../"

printf "${ICYAN} ""$sucess"" successful tests out of ""$nb_test""\n\n${NC}"
exit 0
