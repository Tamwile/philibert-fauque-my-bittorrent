# include <string.h>
# include <unistd.h>

void aff_err(const char *s)
{
  write(2, s, strlen(s));
  write(2, "\n", 1);
  _exit(1);
}
