#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "bencode.h"
#include "torrent.h"
#include "str.h"
#include "sha1.h"
#include "mybittorrent.h"
#include "file_system.h"

extern struct info g_info;

static struct file_list *init_file(void)
{
  struct file_list *fl = malloc(sizeof (struct file_list));
  if (!fl)
    return NULL;

  fl->path = "";
  fl->len = 0;
  fl->next = NULL;

  return fl;
}

void free_file_list(struct file_list *fl)
{
  struct file_list *tmp = fl;

  while (tmp)
  {
    fl = tmp->next;
    free(tmp);
    tmp = fl;
  }
}


static struct file_list *add_file(struct file_list *fl, char *path,
				  unsigned len)
{
  struct file_list *new_file = init_file();

  new_file->path = path;
  new_file->len = len;

  if (!fl)
    return new_file;

  struct file_list *tmp = fl;
  while (tmp->next)
  {
    tmp = tmp->next;
  }
  tmp->next = new_file;

  return fl;
}


char *create_path(struct bencode *path_list, struct bencode *name)
{
  char *path = calloc(1, 1);
  fusion(&path, name->datas);
  int error = mkdir(path, 0755);
  if (error < 0)
  {
    if (errno != EEXIST)
      err(1, path);
    if (g_info.debug)
      printf("dir already exist\n");
  }

  fusion(&path, "/");

  struct bencode *tmp = path_list->list;
  while (tmp->next)
  {
    fusion(&path, tmp->datas);
    int error = mkdir(path, 0755);
    if (error < 0)
    {
      if (errno != EEXIST)
	err(1, path);
      if (g_info.debug)
	printf("dir already exist\n");
    }

    fusion(&path, "/");
    tmp = tmp->next;
  }
  fusion(&path, tmp->datas);

  return path;
}

int print_files(struct file_list *fl)
{
  if (!fl)
    return 0;

  printf("Architecture Files:\n");

  while (fl)
  {
    printf("%*d: ", 10, fl->len);
    printf("%s\n", fl->path);
    fl = fl->next;
  }
  printf("\n\n");

  return 1;
}

/*
** This function is called once the file list is created
** It creates al the files and fill them with the correct size
** of \0
*/
int fill_files(struct file_list *fl)
{
  if (!fl)
    return 0;

  while (fl)
  {
    int taille = fl->len;
    FILE *fp = fopen(fl->path, "r");
    if (fp)
    {
      fclose(fp);
      fl = fl->next;
      continue;
    }

    fp = fopen(fl->path, "w+");
    if (!fp)
    {
      perror(fl->path);
      return 0;
    }
    if (taille)
    {
      fseek(fp, taille - 1, SEEK_SET);
      fputc('\0', fp);
    }

    fclose(fp);

    fl = fl->next;
  }
  return 1;
}

/*
** create a linked list of the path of the files that have to be downloaded
** plus their length
*/
struct file_list *create_file_list(struct bencode *ben)
{
  struct file_list *fl = NULL;
  unsigned nb_name = 0;
  unsigned nb_path = 0;

  struct bencode **file_list_path = get_etiquette(ben, "path", NULL, &nb_path);
  struct bencode **file_list_name = get_etiquette(ben, "name", NULL, &nb_name);
  nb_path = 0;
  struct bencode **file_list_len = get_etiquette(ben, "length", NULL, &nb_path);

  if (!file_list_path && file_list_name) // if there is only one file
  {
    char *tmp = (*file_list_name)->datas;
    fl = add_file(fl, tmp, (*file_list_len)->datai);
  }
  else // if there are directories
  {
    for (unsigned i = 0; i < nb_path; i++)
    {
      char *tmp = create_path(file_list_path[i], (*file_list_name));
      fl = add_file(fl, tmp, file_list_len[i]->datai);
    }
  }

  free(file_list_name);
  free(file_list_path);
  free(file_list_len);

  fill_files(fl);
  if (g_info.debug)
    print_files(fl);
  return fl;
}

/*
** The file list is ordered
** so, with a given length we can return the concerned file
** and the position of the cursor in this file
*/
struct file_list *len_to_file(struct file_list *fl, size_t len,
			      unsigned long *pos)
{
  unsigned long count = 0;
  while (fl)
  {
    if (!fl->len)
    {
      fl = fl->next;
      continue;
    }

    if (count + fl->len > len)
      break;
    count += fl->len;
    fl = fl->next;
  }
  *pos = len - count;
  return fl;
}

char *get_vector(struct file_list *fl, struct torrent *trt)
{
  unsigned nb_byte = trt->nb_piece / 8;
  if (trt->nb_piece % 8)
    nb_byte++;
  char *vector = calloc(nb_byte, 1);

  for (int num = 0; num < trt->nb_piece; num++)
  {
    unsigned byte = num / 8;
    if (valid_piece(fl, num, trt))
    {
      vector[byte] = set_bit(vector[byte], 8 - 1 - (num % 8));
      trt->done += 1;

      if (g_info.debug)
	printf("FOUND VALID PIECE\n");
    }
  }

  return vector;
}
