#ifndef FILE_SYSTEM_H
# define FILE_SYSTEM_H

struct bencode;
struct torrent;

struct file_list {
  char *path;
  unsigned len;
  struct file_list *next;
};

void free_file_list(struct file_list *fl);
struct file_list *create_file_list(struct bencode *ben);
struct file_list *len_to_file(struct file_list *fl, size_t len,
			      unsigned long *pos);
char *get_vector(struct file_list *fl, struct torrent *trt);


#endif /* !FILE_SYSTEM_H */




