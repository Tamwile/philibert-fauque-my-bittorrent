# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "bencode.h"
# include "str.h"
# include "peers.h"

static struct peers_list *init_peer(void)
{
  struct peers_list *out = malloc(sizeof (struct peers_list));
  if (!out)
    return NULL;
  
  out->port = 0;
  for (unsigned ite = 0; ite < 4; ite++)
    (out->ip)[ite] = 0;

  out->state = LINKED;
  out->time = 0;
  out->used = 0;
  out->choke = 1;
  out->interested = 0;
  out->vector = 0;
  out->block = 0;
  out->piece = -1;

  out->msg = NULL;
  out->wrote = 0;
  out->next = NULL;
  return out;
}

static struct peers_list *add_peer0(struct peers_list *list,
				    unsigned char ip[4], unsigned port)
{
  struct peers_list *new = init_peer();
  
  for (unsigned ite = 0; ite < 4; ite++)
    (new->ip)[ite] = ip[ite];
  
  new->port = port;
  
  if (!list)
    return new;
  
  struct peers_list *tmp = list;
  while (tmp->next)
    tmp = tmp->next;

  tmp->next = new;
  return list;
}

static struct peers_list *add_peer1(struct peers_list *list, char *data)
{
  struct peers_list *new = init_peer();
  
  for (unsigned ite = 0; ite < 4; ite++)
    (new->ip)[ite] = data[ite];

  unsigned char b4 = data[4];
  unsigned char b5 = data[5];
  unsigned short port = 0;
  port = b4 << 8;
  port += b5;
  new->port = port;
  
  if (!list)
    return new;
  
  struct peers_list *tmp = list;
  while (tmp->next)
    tmp = tmp->next;

  tmp->next = new;
  return list;
}

void free_peers_list(struct peers_list *list)
{
  struct peers_list *tmp = list;
  while (tmp)
  {
    list = tmp->next;
    free(tmp);
    tmp = list;
  }
}

static struct peers_list *peers_compact0(struct bencode *ben)
{
  unsigned nb = 0;
  struct bencode **ip_list = get_etiquette(ben, "ip", NULL, &nb);
  nb = 0;
  struct bencode **port_list = get_etiquette(ben, "port", NULL, &nb);
  
  struct peers_list *out = NULL;
  for (unsigned ite = 0; ite < nb; ite++)
  {
    char *iptxt = (ip_list[ite])->datas;
    unsigned char ip[4] = { 0 };

    char *tmp = strtok(iptxt, ".");
    for (unsigned i = 0; i < 4; i++)
    {
      unsigned char sum = 0;
      for (; *tmp; tmp++)
	sum = 10 * sum + *tmp - '0';

      ip[i] = sum;
      tmp = strtok(NULL, ".");
    }

    out = add_peer0(out, ip, (port_list[ite])->datai);
  }
  
  free(ip_list);
  free(port_list);
  return out;
}

struct peers_list *load_peers(struct bencode *ben)
{
  unsigned nb = 0;
  struct bencode **list = get_etiquette(ben, "peers", NULL, &nb);

  if (!list)
    return NULL;
  
  if ((*list)->tag == LIST)
  {
    free(list);
    return peers_compact0(ben);
  }
  
  struct peers_list *out = NULL;
  char *all_peers = (list[0])->datas;
  for (unsigned ite = (list[0])->len; ite; ite -= 6)
  {
    out = add_peer1(out, all_peers);
    all_peers += 6;
  }
  free(list);
  return out;
}

int print_peers(struct peers_list *list)
{
  if (!list)
    return 0;
  while (list)
  {
    for (unsigned ite = 0; ite < 4; ite++)
    {
      unsigned char nb = (list->ip)[ite];
      char *txt = uint_to_str(nb);
      printf("%s", txt);
      if (ite != 3)
	printf(".");
      free(txt);
    }
    printf(":%u\n", list->port);
    list = list->next;
  }
  return 1;
}
