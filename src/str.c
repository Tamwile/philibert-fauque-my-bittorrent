#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "str.h"

int same(const char *s1, const char *s2)
{
  unsigned len = (!s1) ? 0 : strlen(s1);
  unsigned len2 = (!s2) ? 0 : strlen(s2);
  if (len != len2)
    return 0;
  for (unsigned ite = 0; ite < len; ite++)
  {
    if (s1[ite] != s2[ite])
      return 0;
  }
  return 1;
}

void fusion(char **out, char *b)
{
  int len_a = strlen(*out);
  int len = len_a + strlen(b);

  *out = realloc(*out, sizeof (char) * (len + 1));
  if (!(*out))
    err(1, "failed memory allocation");
  
  for (int ite = len_a; ite < len; ite++)
    (*out)[ite] = b[ite - len_a];
  (*out)[len] = '\0';
}

char *save_str(char *s)
{
  unsigned len = strlen(s);
  char *save = calloc(len + 1, 1);
  if (!save)
    err(1, "failed allocation");
  for (unsigned ite = 0; ite < len; ite++)
    *(save + ite) = s[ite];
  return save;
}

char *uint_to_str(unsigned nb)
{
  unsigned tmp = nb;
  unsigned log = 0;
  for (; tmp >= 1; tmp /= 10)
    log++;

  if (!nb)
    log++;
  
  char *out = malloc(log + 1);
  if (!out)
    return NULL;

  if (!nb)
  {
    out[0] = '0';
    out[1] = 0;
    return out;
  }
  
  for (unsigned ite = 0; ite < log; ite++)
  {
    out[log - ite - 1] = nb % 10 + '0';
    nb = (nb - nb % 10) / 10;
  }
  out[log] = 0;
  return out;
}

unsigned is_bit_set(unsigned value, unsigned char n)
{
  unsigned mask = 1 << n;
  return (value & mask) != 0;
}

unsigned set_bit(unsigned value, unsigned n)
{
  unsigned mask = 1 << n;
  return value | mask;
}
