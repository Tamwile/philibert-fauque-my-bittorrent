#ifndef READ_TORRENT_H
# define READ_TORRENT_H

enum type
{
  FILES,
  STRG
};

struct read_char
{
  int tag;
  FILE *file;
  char *ptr;
  unsigned cursor;
};

struct info_dictio;

void set_txt(char *s);
void set_file(FILE *f);
char get_next_char(void);
char *get_info_char(char *torrent, struct info_dictio dico);

#endif /* !READ_TORRENT_H */
