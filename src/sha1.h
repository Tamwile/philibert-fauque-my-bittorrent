#ifndef SHA1_H
# define SHA1_H

struct torrent;
struct file_list;

void *compute_sha1(char *mess, unsigned len);
int valid_piece(struct file_list *fl, unsigned num, struct torrent *trt);

#endif /* !SHA1_H */
