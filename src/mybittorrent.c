# include <stdio.h>
# include <stdlib.h>

# include "str.h"
# include "sha1.h"
# include "torrent.h"
# include "bencode.h"
# include "read_torrent.h"
# include "tracker.h"
# include "file_system.h"
# include "mybittorrent.h"

struct info g_info =
{
  .json = 0,
  .dpeers = 0,
  .verbose = 0,
  .seed = 0,
  .debug = 0,
  .torrent = NULL
};

static int get_option_id(const char *s)
{
  char tab[5][28] =
  {
    "--pretty-print-torrent-file",
    "--dump-peers",
    "--verbose",
    "--seed",
    "--debug"
  };

  for (int ite = 0; ite < 5; ite++)
  {
    if (same(tab[ite], s))
      return ite;
  }
  return -1;
}

static char **read_option(int *argc, char **argv)
{
  while (argc)
  {
    switch (get_option_id(*argv))
    {
    case -1:
      return argv;
    case 0:
      g_info.json = 1;
      break;
    case 1:
      g_info.dpeers = 1;
      break;
    case 2:
      g_info.verbose = 1;
      break;
    case 3:
      g_info.seed = 1;
      break;
    case 4:
      g_info.debug = 1;
      break;
    }
    *argc -= 1;
    argv += 1;
  }
  return NULL;
}

static unsigned long length_files(struct bencode *ben)
{
  unsigned nb = 0;
  struct bencode **list = get_etiquette(ben, "length", NULL, &nb);

  unsigned long len = 0;
  for (unsigned ite = 0; ite < nb; ite++)
    len += (list[ite])->datai;
  free(list);
  return len;
}

static struct torrent init_torrent(struct bencode *ben, struct file_list *fl)
{
  unsigned nb = 0;
  struct bencode **list = get_etiquette(ben, "piece length", NULL, &nb);
  unsigned piece_length = (*list)->datai;
  free(list);

  nb = 0;
  list = get_etiquette(ben, "pieces", NULL, &nb);
  char *pieces_hash = (*list)->datas;
  free(list);

  unsigned total_length = length_files(ben);
  unsigned nb_piece = total_length - (total_length % piece_length);
  nb_piece = nb_piece / piece_length;
  nb_piece++;

  unsigned nb_byte = (nb_piece - (nb_piece % 8)) / 8 + 1;
  if (!(nb_piece % 8))
    nb_byte--;
  struct torrent trnt =
  {
    .peer_id = "-MB2020-PFandCRcorpo",
    .pieces_hash = pieces_hash,
    .done = 0,
    .file_list = fl,
    .piece_length = piece_length,
    .total_length = total_length,
    .block_size = (1 << 14),
    .nb_piece = nb_piece,
    .nb_byte = nb_byte,
    .torrent_id = { 0 }
  };
  trnt.blk_p = piece_length / trnt.block_size;
  trnt.vector = get_vector(fl, &trnt);
  
  return trnt;
}

static struct bencode *analyse_torrent(char *torrent, struct info_dictio *dico)
{
  FILE *f = fopen(torrent, "r");
  if (!f)
    exit(1);

  set_file(f);
  
  dico->index_dico = 0;
  dico->length_dico = 0;
  dico->cursor = 0;
  struct bencode *dictionary = parse_bencode(dico, 0);
  fclose(f);

  if (g_info.json)
  {
    print_bencode(dictionary, 0, 0, 1);
    free_bencode(dictionary, 0);
    exit(0);
  }

  return dictionary;
}

int main(int argc, char **argv)
{
  argc--;
  
  char **torrent = read_option(&argc, argv + 1);
  if (!torrent)
    return 1;

  struct info_dictio dico;
  struct bencode *dictionary = analyse_torrent(*torrent, &dico);

  struct file_list *fl = create_file_list(dictionary);
  
  struct torrent trnt = init_torrent(dictionary, fl);
  g_info.torrent = &trnt;

  char *info = get_info_char(*torrent, dico);
  char *sha1 = compute_sha1(info, dico.length_dico);
  free(info);
  for(unsigned ite = 0; ite < 3; ite++)
  {
    unsigned char tmp = sha1[ite];
    sprintf(trnt.torrent_id + ite * 2, "%02x", tmp);
  }
  (trnt.torrent_id)[6] = 0;
  trnt.info_hash = sha1;

  struct tracker_get_request *trck = NULL;
  int event = EMPTY;
  while (trnt.done != trnt.nb_piece)
  {
    trck = send_track_req(dictionary, &trnt, event);
    event = STARTED;
  }

  free(trnt.info_hash);
  free(trnt.vector);
  free(trck);
  free_file_list(fl);
  free_bencode(dictionary, 0);
  return 0;
}
