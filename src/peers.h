#ifndef PEERS_H
# define PEERS_H

enum state
{
  LINKED,
  CONTACTED,
  HANDSHAKE,
  BITFIELD,
  READY,
  REQUEST,
  WRITING,
  TERMINATED
};

# include <poll.h>

struct peers_list
{
  int state;
  unsigned time;
  int used;
  int choke;
  int interested;
  struct pollfd fds;
  
  unsigned char *vector;
  unsigned char ip[4];
  unsigned short port;
  unsigned block;
  int piece;

  char *msg;
  unsigned wrote;
  struct peers_list *next;
};

struct bencode;

struct peers_list *load_peers(struct bencode *ben);
int print_peers(struct peers_list *list);
void free_peers_list(struct peers_list *list);

#endif /* !PEERS_H */
