#ifndef SOCKET_H
# define SOCKET_H

# define TIME_OUT 3000
# define MAX_PEERS 10
# define MAX_PEER_TIME 10

struct sockets
{
  unsigned used;
  struct peers_list *next_peer;
  struct peers_list *selected[MAX_PEERS];
};

struct torrent;

void download_files(struct peers_list *peers, struct torrent *trt);

#endif /* !SOCKET_H */
