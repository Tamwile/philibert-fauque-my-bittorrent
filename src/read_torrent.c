
# include <stdio.h>
# include <stdlib.h>

# include "bencode.h"
# include "mybittorrent.h"
# include "read_torrent.h"

static struct read_char stream =
{
  .tag = FILES,
  .file = NULL,
  .ptr = NULL,
  .cursor = 0
};

char *get_info_char(char *torrent, struct info_dictio dico)
{
  FILE *f = fopen(torrent, "r");
  if (!f)
    return NULL;

  fseek(f, dico.index_dico, SEEK_SET);

  char *out = calloc(dico.length_dico + 1, 1);
  if (!out)
    return NULL;
  for (unsigned ite = 0; ite < dico.length_dico; ite++)
    out[ite] = fgetc(f);

  fclose(f);

  return out;
}

void set_txt(char *s)
{
  stream.tag = STRG;
  stream.ptr = s;
  stream.cursor = 0;
}

void set_file(FILE *f)
{
  stream.tag = FILES;
  stream.file = f;  
}

char get_next_char(void)
{
  if (stream.tag == FILES)
    return fgetc(stream.file);
  stream.cursor += 1;
  return stream.ptr[stream.cursor - 1];
}
