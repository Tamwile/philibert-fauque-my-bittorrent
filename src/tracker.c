#include <curl/curl.h>
#include <string.h>
#include <stdlib.h>

#include "mybittorrent.h"
#include "read_torrent.h"
#include "torrent.h"
#include "peers.h"
#include "str.h"
#include "sha1.h"
#include "bencode.h"
#include "socket.h"
#include "tracker.h"
#include "file_system.h"


extern struct info g_info;

struct tracker_get_request *init_tracker(struct bencode *data,
					 struct torrent *trt, int event)
{
  struct tracker_get_request *out = malloc(sizeof (struct tracker_get_request));
  if (!out)
    return NULL;

  unsigned len = 0;
  struct bencode **list = get_etiquette(data, "announce", NULL, &len);
  out->announce = (list[0])->datas;
  free(list);

  out->port = "6881";

  out->uploaded = 0;
  out->downloaded = 0;
  out->left = trt->total_length;

  out->event = event;
  return out;
}

char *check_ip(struct ip ip)
{
  return NULL;

  if (!strlen(ip.ip) && !strlen(ip.dns))
    return NULL;

  char *txt = calloc(1, 1);
  fusion(&txt, "ip=");

  (!strlen(ip.ip)) ? fusion(&txt, ip.dns) : fusion(&txt, ip.ip);

  fusion(&txt, "&");
  return txt;
}

char *check_event(enum event event)
{
  if (event == STARTED)
    return "started";
  else if (event == COMPLETED)
    return "completed";
  else if (event == STOPPED)
    return "stopped";
  return "empty";
}


char *create_request_url(struct tracker_get_request *req, struct torrent *trt)
{
  char *request_url = calloc(1, 1);
  fusion(&request_url, req->announce);
  fusion(&request_url, "?info_hash=");
  char *escaped = curl_escape(trt->info_hash, 20);
  fusion(&request_url, escaped);
  free(escaped);
  fusion(&request_url, "&peer_id=");
  fusion(&request_url, trt->peer_id);
  fusion(&request_url, "&");
  char *ip = check_ip(req->ip);
  if (ip)
  {
    fusion(&request_url, ip);
    free(ip);
  }
  fusion(&request_url, "port=");
  fusion(&request_url, req->port);
  fusion(&request_url, "&uploaded=");
  char *tmp = uint_to_str(req->uploaded);
  fusion(&request_url, tmp);
  free(tmp);
  fusion(&request_url, "&downloaded=");
  tmp = uint_to_str(req->downloaded);
  fusion(&request_url, tmp);
  free(tmp);
  fusion(&request_url, "&left=");
  tmp = uint_to_str(req->left);
  fusion(&request_url, tmp);
  free(tmp);
  if (req->event != EMPTY)
  {
    fusion(&request_url, "&event=");
    fusion(&request_url, check_event(req->event));
  }
  return request_url;
}

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
  nmemb = nmemb;
  set_txt(ptr);
  struct bencode *ben = parse_bencode(NULL, 0);

  if (g_info.debug)
    print_bencode(ben, 0, 0, 1);

  struct peers_list *peers = load_peers(ben);
  if (g_info.dpeers)
  {
    free_peers_list(peers);
    free_bencode(ben, 0);
    return print_peers(peers);
  }
  
  download_files(peers, userdata);

  free_bencode(ben, 0);
  free_peers_list(peers);
  return size;
}

struct tracker_get_request *send_track_req(struct bencode *ben,
					   struct torrent *trt, int event)
{
  struct tracker_get_request *req = init_tracker(ben, trt, event);
  char *url = create_request_url(req, trt);

  CURL *handle = curl_easy_init();
  char errbuf[CURL_ERROR_SIZE];

  if (g_info.verbose)
  {
    printf("%s: tracker: requesting peers to %s\n",
	   trt->torrent_id, req->announce);
  }

  if (g_info.debug)
    printf("URL:%s\n", url);
  curl_easy_setopt(handle, CURLOPT_URL, url);
  curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_callback);
  curl_easy_setopt(handle, CURLOPT_WRITEDATA, trt);
  curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, &errbuf);
  curl_easy_perform(handle);
  curl_easy_cleanup(handle);
  curl_global_cleanup();
  free(url);
  return req;
}
