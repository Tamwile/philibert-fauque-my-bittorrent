#ifndef BENCODE_H
# define BENCODE_H

# define TAB 4

enum tag
{
  END = 0,
  STR = 1,
  INT = 2,
  LIST = 3,
  DICT = 4  
};

struct bencode
{
  int tag;
  char *datas;
  unsigned len;
  unsigned long datai;
  char *key;
  struct bencode *list;
  struct bencode *next;
};

struct info_dictio
{
  unsigned index_dico;
  unsigned length_dico;
  unsigned cursor;
};

struct bencode *parse_bencode(struct info_dictio *dico, int is_dict);
void free_bencode(struct bencode *ben, int in_dico);
void print_bencode(struct bencode *ben, unsigned decal, int dico, int last);
struct bencode **get_etiquette(struct bencode *ben, char *etiquette,
			       struct bencode **last, unsigned *nb);

#endif /* !BENCODE_H */
