# include <stdio.h>
# include <stdlib.h>

# include "str.h"
# include "read_torrent.h"
# include "bencode.h"

struct bencode *init_bencode(void)
{
  struct bencode *out = malloc(sizeof (struct bencode));
  if (!out)
    return NULL;
  out->tag = -1;
  out->datas = "";
  out->datai = 0;
  out->list = NULL;
  out->key = "";
  out->next = NULL;
  return out;
}

char *get_str(unsigned len, struct info_dictio *dico)
{
  char *out = calloc(len + 1, 1);
  for (unsigned ite = 0; ite < len; ite++)
  {
    out[ite] = get_next_char();
    if (dico)
      dico->cursor += 1;
  }
  
  return out;
}

int get_int(int *is_int, struct info_dictio *dico)
{
  char digit = get_next_char();
  if (dico)
    dico->cursor += 1;
  *is_int = 1;
  if (digit == 'i' || digit ==  'l' || digit ==  'd' || digit == 'e')
    *is_int = 0;
  
  int neg = 0;
  switch (digit)
  {
  case 'e':
    return END;
  case 'i':
    return INT;
  case 'l':
    return LIST;
  case 'd':
    return DICT;
  case '-':
    neg = 1;
    digit = get_next_char();
    if (dico)
      dico->cursor += 1;
  }
  
  int nb = 0;
  while (digit >= '0' && digit <= '9')
  {
    nb = 10 * nb + digit - '0';
    digit = get_next_char();
    if (dico)
      dico->cursor += 1;
  }
  
  return (neg) ? -nb : nb;
}

int get_type(unsigned *strlen, struct info_dictio *dico)
{
  int is_int = 0;
  int out = get_int(&is_int, dico);
  
  if (is_int)
  {
    *strlen = out;
    return STR;
  }
  
  return out;
}

struct bencode *fill_dico_key(int *found_info, struct info_dictio *dico,
			      int is_dict)
{
  struct bencode *ele = NULL;
  if (is_dict)
  {
    ele = parse_bencode(dico, 0);
    if (!ele)
      return NULL;
    ele->key = ele->datas;
    
    if (same(ele->key, "info"))
    {
      dico->index_dico = dico->cursor;
      *found_info = 1;
    }
  }
  else
    ele = init_bencode();
  return ele;
}

struct bencode *fill_list_dict(struct bencode *ele, struct info_dictio *dico,
			       int is_dict)
{
  if (is_dict)
    ele->tag = DICT;
  else
    ele->tag = LIST;

  struct bencode *next = parse_bencode(dico, is_dict);
  ele->list = next;
  struct bencode *tmp = next;
  while (tmp)
  {
    next = parse_bencode(dico, is_dict);
    tmp->next = next;
    tmp = next;
  }
  return ele;
}

struct bencode *parse_bencode(struct info_dictio *dico, int is_dict)
{
  int found_info = 0;
  //struct bencode *ele = fill_dico_key(&found_info, dico, is_dict);
  
  struct bencode *ele = NULL;
  if (is_dict)
  {
    ele = parse_bencode(dico, 0);
    if (!ele)
      return NULL;
    ele->key = ele->datas;
    
    if (dico && same(ele->key, "info"))
    {
      dico->index_dico = dico->cursor;
      found_info = 1;
    }
  }
  else
    ele = init_bencode();
  
  unsigned len = 0;
  switch (get_type(&len, dico))
  {
  case END:
  {
    free(ele);
    return NULL;
  }
  case STR:
    ele->tag = STR;
    ele->len = len;
    ele->datas = get_str(len, dico);
    return ele;
  case INT:
    ele->tag = INT;
    int is_int = 0;
    ele->datai = get_int(&is_int, dico);
    return ele;
  case LIST:
    return fill_list_dict(ele, dico, 0);
  case DICT:
    ;
    struct bencode *out = fill_list_dict(ele, dico, 1);
    if (found_info && dico)
      dico->length_dico = dico->cursor - dico->index_dico;
    return out;
  }
  
  return NULL;
}

void print_str(int dico, int decal, char *data, unsigned len)
{
  printf("%*s\"", dico * decal, "");
  for (unsigned ite = 0; ite < len; ite++)
  {
    if (*data < 0x20 || *data > 0x7E)
    {
      printf("\\\\u00");
      char out[9] = { 0 };
      sprintf(out, "%2x", *data);
      out[0] = out[6];
      out[1] = out[7];
      printf("%.2s", out);
    }
    else if (*data == '"' || *data == '\\')
      printf("\\%c", *data);
    else
      printf("%c", *data);
    data++;
  }
  printf("\"");
}

void free_bencode(struct bencode *ben, int in_dico)
{
  if (!ben)
    return;
  
  if (in_dico)
    free(ben->key);
  
  if (ben->tag == STR)
    free(ben->datas);

  if (ben->tag == DICT || ben->tag == LIST)
  {
    struct bencode *tmp = ben->list;
    while (tmp)
    {
      struct bencode *tmp2 = tmp->next;
      free_bencode(tmp, ben->tag == DICT);
      tmp = tmp2;
    }
  }
  free(ben);
}

void print_bencode(struct bencode *ben, unsigned decal, int dico, int last)
{
  if (dico)
    printf("%*s\"%s\": ", decal, "", ben->key);
  
  if (ben->tag == LIST || ben->tag == DICT)
  {
    if (ben->tag == DICT)
      printf("%*s{\n", !dico * decal, "");
    else
      printf("%*s[\n", !dico * decal, "");
    
    int dico = ben->tag == DICT;    
    struct bencode *tmp = ben->list;
    while (tmp)
    {
      print_bencode(tmp, decal + TAB, dico, tmp->next == NULL);
      tmp = tmp->next;
    }

    if (dico)
      printf("%*s}", decal, "");
    else
      printf("%*s]", decal, "");
  }

  if (ben->tag == INT)
    printf("%*s%ld", !dico * decal, "", ben->datai);
  
  if (ben->tag == STR)
    print_str(!dico, decal, ben->datas, ben->len);
  
  if (!last)
    printf(",");
  printf("\n");
}

struct bencode **get_etiquette(struct bencode *ben, char *etiquette,
			       struct bencode **last, unsigned *nb)
{
  if (same(ben->key, etiquette))
  {
    *nb += 1;
    if (*nb == 1)
    {
      last = malloc(sizeof (struct bencode *));
      last[0] = ben;
      return last;
    }
    last = realloc(last, sizeof (struct bencode *) * *nb);
    last[*nb - 1] = ben;
    return last;
  }
  
  if (ben->tag == LIST || ben->tag == DICT)
  {
    struct bencode *tmp = ben->list;
    while (tmp)
    {
      last = get_etiquette(tmp, etiquette, last, nb);
      tmp = tmp->next;
    }
  }
  return last;
}
