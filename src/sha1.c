#include <fcntl.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "str.h"
#include "file_system.h"
#include "torrent.h"
#include "sha1.h"

void *compute_sha1(char *mess, unsigned len)
{
  unsigned char *md_value = malloc(21);
  if (!md_value)
    return NULL;
  unsigned int md_len = 0;
  EVP_MD_CTX *mdctx = EVP_MD_CTX_new();

  EVP_DigestInit_ex(mdctx, EVP_sha1(), NULL);
  EVP_DigestUpdate(mdctx, mess, len);
  EVP_DigestFinal_ex(mdctx, md_value, &md_len);
  EVP_MD_CTX_free(mdctx);

  md_value[20] = '\0';
  return md_value;
}

static int load_piece(struct file_list *f_list, char *msg, unsigned long len,
	       unsigned long pos)
{
  unsigned long cursor = 0;
  while (cursor < len)
  {
    if (!f_list)
      return 0;

    int nb_read = f_list->len - pos;
    if (f_list->len - pos > len - cursor)
      nb_read = len - cursor;
    if (nb_read <= 0)
    {
      f_list = f_list->next;
      pos = 0;
      continue;
    }

    int fd = open(f_list->path, O_RDONLY);
    if (fd < 0)
    {
      free(msg);
      return 0;
    }
    lseek(fd, pos, SEEK_SET);
    pos = 0;

    if (read(fd, msg + cursor, nb_read) != nb_read)
    {
      free(msg);
      return 0;
    }
    cursor += nb_read;
    close(fd);
    f_list = f_list->next;
  }
  return 1;
}

static int compare_hash(struct torrent *trt, unsigned num, char *myhash)
{
  char *pieces_hash = trt->pieces_hash;
  unsigned offset = num * 20;

  for (unsigned ite = 0; ite < 20; ite++)
  {
    if (pieces_hash[offset + ite] != myhash[ite])
      return 0;
  }
  return 1;
}

int valid_piece(struct file_list *fl, unsigned num, struct torrent *trt)
{
  unsigned long decal = num * trt->piece_length;
  unsigned len = trt->piece_length;
  if (trt->total_length < decal + trt->piece_length)
    len = trt->total_length - decal;

  char *msg = malloc(len);
  if (!msg)
    return 0;
  unsigned long pos = 0;
  struct file_list *f_list = len_to_file(fl, decal, &pos);

  if (!load_piece(f_list, msg, len, pos))
    return 0;

  char *myhash = compute_sha1(msg, len);
  free(msg);
  if (!myhash)
    return 0;

  int out = compare_hash(trt, num, myhash);

  free(myhash);
  return out;
}
