# include <arpa/inet.h>
# include <errno.h>
# include <poll.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/socket.h>
# include <sys/types.h>

# include "str.h"
# include "messages.h"
# include "mybittorrent.h"
# include "torrent.h"
# include "peers.h"
# include "socket.h"

extern struct info g_info;

static int send_handshake(struct peers_list *peer, struct torrent *trt)
{
  struct pollfd fds = peer->fds;
  char msg[68] = { 0 };
  char *pstr = "BitTorrent protocol";
  msg[0] = 19;

  unsigned offset = 1;
  for (unsigned ite = 0; ite < 19; ite++)
    msg[ite + offset] = pstr[ite];

  offset += 19 + 8;
  char *hash = trt->info_hash;
  for (unsigned ite = 0; ite < 20; ite++)
    msg[ite + offset] = hash[ite];

  offset += 20;
  char *p_id = trt->peer_id;
  for (unsigned ite = 0; ite < 20; ite++)
    msg[ite + offset] = p_id[ite];

  int nb = send(fds.fd, msg, 68, MSG_DONTWAIT);
  if (nb != 68)
    return 0;

  if (g_info.verbose)
    printf("%s: msg: send: %d.%d.%d.%d:%d: handshake\n", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port);
  peer->state = CONTACTED;
  return 1;
}

static int rcvd_handshake(struct peers_list *peer, struct torrent *trt)
{
  struct pollfd fds = peer->fds;
  char msg[68] = { 0 };

  int nb = recv(fds.fd, msg, 68, MSG_DONTWAIT);
  if (nb != 68)
    return 0;

  if (g_info.verbose)
    printf("%s: msg: recv: %d.%d.%d.%d:%d: handshake\n", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port);

  for (unsigned ite = 0; ite < 20; ite++)
  {
    if (msg[ite + 1 + 19 + 8] != trt->info_hash[ite])
    {
      if (g_info.debug)
	printf("BAD INFO-HASH\n");
      return 0;
    }
  }

  peer->state = HANDSHAKE;
  return 1;
}

static int type_case(struct peers_list *peer, char type, struct torrent *trt)
{
  switch (type)
  {
  case 0:
    if (g_info.verbose)
      printf("%s: msg: recv: %d.%d.%d.%d:%d: choke\n", trt->torrent_id,
	     peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port);
    peer->choke = 1;
    return 1;
  case 1:
    if (g_info.verbose)
      printf("%s: msg: recv: %d.%d.%d.%d:%d: unchoke\n", trt->torrent_id,
	     peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port);
    peer->choke = 0;
    return 1;
  case 2:
    peer->interested = 1;
    return 1;
  case 3:
    peer->interested = 0;
    return 1;
  case 4:
    return have_piece(peer, trt);
  case 5:
    return fill_bitfield(peer, trt);
  case 6:
    return 1;
  case 7:
    return add_piece(peer, trt);
  }
  return 1;
}

static int pollin_event(struct peers_list *peer, struct pollfd fds,
		 struct torrent *trt)
{
  peer->time = 0;
  if (peer->state == CONTACTED)
    return rcvd_handshake(peer, trt);
  
  char prefix[5] = { 0 };
  int nb = recv(fds.fd, prefix, 5, MSG_DONTWAIT);
  if (nb != 5)
  {
    printf("BAD RECEIVE  %d\n", nb);
    return 0;
  }
  
  //void *void_pref = prefix;
  //unsigned *len = void_pref;
  //unsigned length = ntohl(*len);
  char type = prefix[4];
  
  return type_case(peer, type, trt);
}

static int read_event(struct peers_list *peer, struct sockets *skts,
		      struct torrent *trt)
{
  struct pollfd fds = peer->fds;
  if (peer->state == WRITING)
    return add_piece(peer, trt);

  if (!fds.revents)
    return 1;

  if ((fds.revents & POLLIN) == POLLIN || (fds.revents & POLLPRI) == POLLPRI)
    return pollin_event(peer, fds, trt);

  if ((fds.revents & POLLOUT) == POLLOUT)
  {
    unsigned time = peer->time;
    peer->time = 0;
    if (peer->state == LINKED)
      return send_handshake(peer, trt);
    if (peer->state == BITFIELD)
      return choose_piece(peer, skts, trt, 0);
    if (peer->state == READY && !peer->choke)
      return choose_piece(peer, skts, trt, 1);
    peer->time = time;
  }
  if ((fds.revents & POLLNVAL) == POLLNVAL || (fds.revents & POLLERR) == POLLERR
      || (fds.revents & POLLHUP) == POLLHUP)
  {
    if (g_info.debug)
      printf("Deconnected !\n");
    return 0;
  }
  return 1;
}

int init_socket(struct peers_list *peers, struct torrent *trt)
{
  int fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (fd < 0)
    return -1;

  int on = 1;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1)
    return -1;

  void *tmp = peers->ip;
  unsigned *long_addr = tmp;
  struct in_addr in_ad;
  in_ad.s_addr = *long_addr;

  struct sockaddr_in addr =
  {
    .sin_family = AF_INET,
    .sin_port = htons(peers->port),
    .sin_addr = in_ad
  };

  void *address = &addr;
  if (connect(fd, address, sizeof (struct sockaddr_in)) == -1)
  {
    if (errno != EINPROGRESS)
      return -1;
  }

  if (g_info.verbose)
    printf("%s: peers: connect: %d.%d.%d.%d:%d\n", trt->torrent_id,
	   peers->ip[0], peers->ip[1], peers->ip[2], peers->ip[3], peers->port);

  peers->state = LINKED;
  return fd;
}

static int load_poll(struct sockets *skts, struct pollfd *fds_list)
{
  int out = poll(fds_list, skts->used, TIME_OUT);
  if (out <= 0)
  {
    for (unsigned ite = 0; ite < MAX_PEERS; ite++)
    {
      if ((skts->selected)[ite])
	((skts->selected)[ite])->time += 1;
    }
    if (g_info.debug)
      printf("TIME_OUT\n");
  }
  for (unsigned ite = 0; ite < MAX_PEERS; ite++)
  {
    for (unsigned i = 0; i < MAX_PEERS; i++)
    {
      if (!(skts->selected)[ite])
	continue;
      if (fds_list[ite].fd == (((skts->selected)[ite])->fds).fd)
      {
	((skts->selected)[ite])->fds = fds_list[ite];
	break;
      }
    }
  }
  return 1;
}

static void connect_peers(struct sockets *skts, struct pollfd *fds_list,
			  struct torrent *trt)
{
  if (!load_poll(skts, fds_list))
    return;

  for (unsigned ite = 0; ite < MAX_PEERS; ite++)
  {
    if (!(skts->selected)[ite])
      continue;
    if (!read_event((skts->selected)[ite], skts, trt) ||
	((skts->selected)[ite])->time > MAX_PEER_TIME)
    {
      if (g_info.verbose)
      {
	struct peers_list *peers = (skts->selected)[ite];
	printf("%s: peers: disconnect: %d.%d.%d.%d:%d\n", trt->torrent_id,
	       peers->ip[0], peers->ip[1], peers->ip[2], peers->ip[3],
	       peers->port);
      }
      (skts->selected)[ite] = NULL;
      skts->used -= 1;
    }
    //else
    //((skts->selected)[ite]->fds).revents = 0;
  }
}

static void load_sockets(struct sockets *skts, struct torrent *trt)
{
  for (; skts->used < MAX_PEERS && skts->next_peer; skts->used += 1)
  {
    for (unsigned ite = 0; ite < MAX_PEERS; ite++)
    {
      if (!((skts->selected)[ite]))
      {
	(skts->selected)[ite] = skts->next_peer;
	int fd = init_socket(skts->next_peer, trt);
	if (fd < 0)
	{
	  skts->next_peer = (skts->next_peer)->next;
	  ite--;
	  break;
	}

	struct pollfd fds =
	{
	  .fd = fd,
	  .events = POLLIN | POLLOUT,
	  .revents = 0
	};
	(skts->next_peer)->fds = fds;
	skts->next_peer = (skts->next_peer)->next;
	break;
      }
    }
  }
}

void download_files(struct peers_list *peers, struct torrent *trt)
{
  struct sockets skts =
  {
    .used = 0,
    .next_peer = peers,
    .selected = { 0 }
  };

  while ((skts.next_peer || skts.used) && trt->done != trt->nb_piece)
  {
    load_sockets(&skts, trt);
    
    struct pollfd fds_list[MAX_PEERS] = { 0 };
    unsigned index = 0;
    for (unsigned ite = 0; ite < skts.used; ite++)
    {
      if (!((skts.selected)[ite]))
	break;
      fds_list[index] = ((skts.selected)[ite])->fds;
      index++;
    }    
    connect_peers(&skts, fds_list, trt);
  }
}
