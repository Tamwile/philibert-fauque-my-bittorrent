#ifndef MYBITTORRENT_H
# define MYBITTORRENT_H

struct info
{
  int json;
  int dpeers;
  int verbose;
  int seed;
  int debug;
  struct torrent *torrent;
};

#endif /* !MYBITTORRENT_H */
