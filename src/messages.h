#ifndef MESSAGES_H
# define MESSAGES_H

struct peers_list;
struct sockets;
struct torrent;

int add_piece(struct peers_list *peer, struct torrent *trt);
int have_piece(struct peers_list *peer, struct torrent *trt);
int choose_piece(struct peers_list *peer, struct sockets *skts,
		 struct torrent *trt, int interested);
int fill_bitfield(struct peers_list *peer, struct torrent *trt);

#endif /* !MESSAGES_H */
