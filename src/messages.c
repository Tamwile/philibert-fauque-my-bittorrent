# include <arpa/inet.h>
# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>

# include "str.h"
# include "sha1.h"
# include "socket.h"
# include "torrent.h"
# include "mybittorrent.h"
# include "peers.h"
# include "file_system.h"
# include "messages.h"

extern struct info g_info;

int write_piece(struct file_list *file, unsigned len, char *msg,
		unsigned long pos)
{
  printf("POS :%ld\n", pos);
  unsigned wrote = 0;
  while (len < wrote)
  {
    int write_size = len - wrote;
    if (len - wrote > file->len - pos)
      write_size = file->len - pos;
    if (write_size <= 0)
    {
      file = file->next;
      pos = 0;
      continue;
    }

    int fd = open(file->path, O_WRONLY);
    if (fd < 0)
    {
      free(msg);
      return 1;
    }

    lseek(fd, pos, SEEK_SET);
    pos = 0;

    printf("LEN:%d wrote:%d wrote_size:%d\n", len, wrote, write_size);
    printf("PATH:%s\n", file->path);
    int nb = write(fd, msg + wrote, write_size);
    if (nb != write_size)
    {
      free(msg);
      close(fd);
      return 1;
    }

    wrote += nb;
    close(fd);
    file = file->next;
  }

  return 0;
}

static void check_piece(struct peers_list *peer, struct torrent *trt,
			unsigned piece, unsigned pos)
{
  if (g_info.verbose)
    printf("%s: msg: recv: %d.%d.%d.%d:%d: piece %d %d\n", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port,
	   piece, pos);

  free(peer->msg);

  peer->msg = NULL;
  peer->block += 1;

  peer->state = READY;
  unsigned lastln = trt->total_length - (trt->nb_piece - 1) * trt->piece_length;
  unsigned last_block = lastln / trt->block_size + 1;
  if (!(lastln % trt->block_size))
    last_block--;
  if (peer->block >= trt->blk_p ||
      (peer->piece + 1 == trt->nb_piece && peer->block == last_block))
  {
    if (valid_piece(trt->file_list, piece, trt))
    {
      unsigned byte = piece / 8;
      (trt->vector)[byte] = set_bit((trt->vector)[byte], 8 - 1 - (piece % 8));
      trt->done += 1;
      if (g_info.debug)
	printf("DOWLOADED VALID PIECE: %d\n", piece);
    }

    peer->block = 0;
    peer->piece = -1;
  }
}

int add_piece(struct peers_list *peer, struct torrent *trt)
{
  int piece = peer->piece;
  if (piece < 0)
  {
    printf("BAD ADD PIECE1\n");
    return 0;
  }

  unsigned long decal = (trt->blk_p * piece + peer->block) * trt->block_size;
  unsigned len = trt->block_size;
  if (trt->total_length < decal + trt->block_size)
    len = trt->total_length - decal;
  int first = 0;
  if (!peer->msg)
  {
    peer->msg = calloc(len + 8, 1);
    peer->wrote = 0;
    first = 1;
  }
  if (!peer->msg)
    return 1;

  unsigned long start_write = decal + peer->wrote;
  unsigned long pos = 0;
  struct file_list *file = len_to_file(trt->file_list, start_write, &pos);

  int nb = -1;
  if (first)
    nb = recv((peer->fds).fd, peer->msg, len + 8, MSG_DONTWAIT);
  else
    nb = recv((peer->fds).fd, peer->msg, len - peer->wrote, MSG_DONTWAIT);
  if (nb < 0)
  {
    free(peer->msg);
    printf("BAD ADD PIECE2\n");
    return 0;
  }

  unsigned out;
  if (first)
    out = write_piece(file, len, peer->msg + 8, pos);
  else
    out = write_piece(file, len, peer->msg, pos);
  if (!out)
  {
    peer->wrote += nb - 8 * first;
    peer->state = WRITING;
    if (peer->wrote == len)
      check_piece(peer, trt, piece, peer->block * trt->block_size);
  }
  return 1;
}

int have_piece(struct peers_list *peer, struct torrent *trt)
{
  char *msg = malloc(4);
  if (!msg)
    return 1;

  int nb = recv((peer->fds).fd, msg, 4, MSG_DONTWAIT);
  if (nb != 4)
  {
    printf("BAD HAVE PIECE\n");
    return 0;
  }

  void *void_msg = msg;
  unsigned *tmp = void_msg;
  unsigned num = ntohl(*tmp);

  unsigned byte = num / 8;
  (peer->vector)[byte] = set_bit((peer->vector)[byte], 8 - 1 - (num % 8));

  if (g_info.verbose)
    printf("%s: msg: recv: %d.%d.%d.%d:%d: have %d\n", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port, num);
  return 1;
}

static int request_block(struct peers_list *peer, struct torrent *trt,
			 unsigned num, unsigned id_block)
{
  char *msg = calloc(5 + 12, 1);
  if (!msg)
    return 1;

  void *void_msg = msg;
  unsigned *tmp = void_msg;
  *tmp = htonl(13);

  msg[4] = 6;

  void_msg = msg + 5;
  tmp = void_msg;
  *tmp = htonl(num);

  unsigned offset = id_block * trt->block_size;
  *(tmp + 1) = htonl(offset);

  unsigned len = trt->block_size;
  if (trt->total_length < (trt->blk_p * num + id_block + 1) * trt->block_size)
    len = trt->total_length -((trt->blk_p * num +id_block) * trt->block_size);
  *(tmp + 2) = htonl(len);

  int nb = send((peer->fds).fd, msg, 17, MSG_DONTWAIT);
  if (nb != 17)
  {
    printf("BAD REQUEST BLOCK\n");
    free(msg);
    return 0;
  }

  if (g_info.verbose)
    printf("%s: msg: send: %d.%d.%d.%d:%d: request %d %d %d\n", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port, num,
	   offset, len);

  peer->block = id_block;
  peer->state = REQUEST;
  free(msg);
  return 1;
}

static int send_interested(struct peers_list *peer, struct torrent *trt)
{
  char *msg = calloc(5, 1);
  if (!msg)
    return 1;

  void *void_msg = msg;
  unsigned *tmp = void_msg;
  *tmp = htonl(1);

  msg[4] = 2;


  int nb = send((peer->fds).fd, msg, 5, MSG_DONTWAIT);
  if (nb != 5)
  {
    printf("BAD SEND_INTERESTED\n");
    free(msg);
    return 0;
  }

  if (g_info.verbose)
    printf("%s: msg: send: %d.%d.%d.%d:%d: interested\n", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port);

  peer->state = READY;
  free(msg);
  return 1;
}

int choose_piece(struct peers_list *peer, struct sockets *skts,
		 struct torrent *trt, int interested)
{
  if (peer->block)
    return (interested) ? request_block(peer, trt, peer->piece, peer->block) :
      send_interested(peer, trt);

  unsigned nb_piece = trt->nb_piece;
  nb_piece += (8 - nb_piece) % 8;
  unsigned nb_byte = trt->nb_byte;

  int num = 0;
  unsigned char *vector = peer->vector;
  struct peers_list **all_peers = skts->selected;

  for (unsigned ite = 0; ite < nb_byte; ite++)
  {
    unsigned char mybyte = (trt->vector)[ite];
    for (unsigned i = 0; i < 8 && num < trt->nb_piece; i++)
    {
      int free = 1;
      for (unsigned tmp = 0; tmp < MAX_PEERS; tmp++)
      {
	if (!all_peers[tmp] || ((all_peers[tmp])->fds).fd == (peer->fds).fd)
	  continue;
	if (all_peers[tmp]->piece == num)
	{
	  free = 0;
	  break;
	}
      }
      if (!is_bit_set(mybyte, 8 - i - 1) && is_bit_set(vector[ite], 8 - i - 1)
	  && free)
      {
	peer->piece = num;
	return (interested) ? request_block(peer, trt, num, 0) :
	  send_interested(peer, trt);
      }
      num++;
    }
  }
  return 1;
}

int fill_bitfield(struct peers_list *peer, struct torrent *trt)
{
  int nb_byte = trt->nb_byte;
  unsigned char *msg = malloc(nb_byte);
  if (!msg)
    return 1;

  int nb = recv((peer->fds).fd, msg, nb_byte, MSG_DONTWAIT);
  if (nb != nb_byte)
  {
    printf("BAD BITFIELD\n");
    return 0;
  }

  if (g_info.verbose)
  {
    printf("%s: msg: recv: %d.%d.%d.%d:%d: bitfield ", trt->torrent_id,
	   peer->ip[0], peer->ip[1], peer->ip[2], peer->ip[3], peer->port);
    for (int ite = 0; ite < nb_byte; ite++)
    {
      unsigned char tmp = msg[ite];
      for (int i = 7; i >= 0; i--)
	printf("%d", is_bit_set(tmp, i));
    }
    printf("\n");
  }

  peer->state = BITFIELD;
  peer->vector = msg;
  return 1;
}
