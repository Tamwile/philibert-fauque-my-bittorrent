#ifndef TRACKER_H
# define TRACKER_H


struct ip
{
  char ip[16];
  char dns[128];
};

enum event
{
  STARTED,
  COMPLETED,
  STOPPED,
  EMPTY
};

struct tracker_get_request
{
  char *announce;
  struct ip ip;
  char *port; // pour l'instant, max_bound 65000 non pris en compte
  unsigned int uploaded;
  unsigned int downloaded;
  unsigned int left;
  enum event event;
};

struct tracker_get_request *send_track_req(struct bencode *ben,
					   struct torrent *trt, int event);

#endif /* !TRACKER_H */

