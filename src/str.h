#ifndef STR_H
# define STR_H

int same(const char *s1, const char *s2);
void fusion(char **out, char *b);
char *save_str(char *s);
char *uint_to_str(unsigned nb);
unsigned is_bit_set(unsigned value, unsigned char n);
unsigned set_bit(unsigned value, unsigned n);

#endif /* !STR_H */
