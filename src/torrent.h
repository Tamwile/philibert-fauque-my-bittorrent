#ifndef TORRENT_H
# define TORRENT_H

struct torrent
{
  char *peer_id;
  char *info_hash;
  char *pieces_hash;
  char *vector;
  int done;
  struct file_list *file_list;
  unsigned piece_length;
  unsigned long total_length;
  unsigned block_size;
  unsigned blk_p;
  int nb_piece;
  unsigned nb_byte;
  char torrent_id [7];
};

#endif /* !TORRENT_H */
